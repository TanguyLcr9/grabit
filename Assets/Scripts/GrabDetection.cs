﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabDetection : MonoBehaviour
{
    public float forceEjection = 60f;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Sbire"))
        {
            //applique une force dans le sens opposé à la collision pour éjecter le sbire
            collision.gameObject.GetComponent<Rigidbody>().AddForce(
                -collision.GetContact(0).normal * forceEjection,
                ForceMode.Impulse);
        }
    }
}
