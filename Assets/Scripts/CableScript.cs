﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableScript : MonoBehaviour
{
    public Transform Root, Grabber;
    LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        // Pour garder le lien entre le Root et le Grab
        // Il n'y a pas de phisique associée à cette ligne car le gant est assez puissant
        lineRenderer.SetPosition(0, Root.position);
        lineRenderer.SetPosition(1, Grabber.position);
    }
}
