﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BonusType // Pour parametrer les prefabs j'utilse cet enum pour définir le type de bonus
{
    Faster,
    Slower,
    Grow,
    Shrink
}

public class BonusSpawner : MonoBehaviour
{
    public static BonusSpawner Instance;
    [Range(0, 30)]
    public float RangeMin, RangeMax;
    public float timerMax;
    public GameObject[] P_Bonus;
    public GameObject[] P_Malus;
    
    float timer;


    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }



    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > timerMax)
        {
            Spawn();
            timer = 0;
        }
    }

    /// <summary>
    /// Spawn un bonus aléatoirement entre bonus et malus et aléatoirement entre eux également
    /// </summary>
    public void Spawn()
    {
        bool bonus = Random.Range(0,2) == 1;//bonus ou malus random

        GameObject goToInstatiate;

        if (bonus)
        {
            goToInstatiate = P_Bonus[Random.Range(0,P_Bonus.Length)];//random parmi les bonus
        }
        else
        {
            goToInstatiate = P_Malus[Random.Range(0, P_Malus.Length)];//random parmi les malus
        }

        GameObject instance = Instantiate(goToInstatiate, SpawnPoint(), Quaternion.identity);
        instance.transform.SetParent(transform);

    }

    /// <summary>
    /// Définir un point de spawn entre un dimètre minimum et maximum
    /// </summary>
    /// <returns></returns>
    Vector3 SpawnPoint()
    {
        Vector3 spawnPoint = new Vector3(RangeMax, 0, 0);
        float RanPosX, RanPosY;

        for (int i = 0; i <= 50; i++)
        {
            RanPosX = Random.Range(-RangeMax, RangeMax);
            RanPosY = Random.Range(-RangeMax, RangeMax);

            if (RangeMin < Vector3.Distance(Vector3.zero, new Vector3(RanPosX, RanPosY, 0))
                && Vector3.Distance(Vector3.zero, new Vector3(RanPosX, RanPosY, 0)) < RangeMax)//Si le vecteur random est bien entre les deux cercles
            {
                spawnPoint = new Vector3(RanPosX, RanPosY, 0);//Alors celui-ci est bon
                break;
            }

            if (i == 49)//Si trop de calculs nécessaires alors on affiche un message
            {
                Debug.Log("Pas de place !!! Il faut laisser plus de place entre RangeMax et RangeMin");
            }
        }

        return spawnPoint;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, RangeMax);//cerlce bleu pour le maximum

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, RangeMin);//cerlce vert pour le minimum

        //Clamper les valeurs
        RangeMin = Mathf.Clamp(RangeMin, 0, RangeMax);
        RangeMax = Mathf.Clamp(RangeMax, RangeMin, 30);

    }
}
