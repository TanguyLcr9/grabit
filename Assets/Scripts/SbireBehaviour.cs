﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Direction //cette classe permet de définir la direction d'impulsion plus facilement du sbire dans ce script
{
    left,
    right,
    up,
    down,
    jump
}

public class SbireBehaviour : MonoBehaviour
{
    public float force;
    public float jumpForce;
    public GameObject Go_SbireMesh;
    public MeshRenderer mesh;

    Coroutine doRespawn;
    Rigidbody rb;
    float timerInactivity;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Color RanColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        mesh.material.color = RanColor;
        mesh.material.SetColor("_EmissionColor", RanColor);
    }

    // Update is called once per frame
    void Update()
    {
        if ((transform.position.z > 10 || transform.position.z <-10) && doRespawn == null)
        {
            doRespawn = StartCoroutine(DoRespawn());
            
        }
        if(timerInactivity > 20)//20 secondes sans activité provoquera la destruction du gameObject avec son nom de joueur
        {
            Destroy(gameObject);
        }
        timerInactivity += Time.deltaTime;
    }

    /// <summary>
    /// Une fois que le joueur est détruit, on le retire de la liste du GameManager pour éviter des erreurs
    /// </summary>
    private void OnDestroy()
    {
        GameManager.Instance.Sbires_List.Remove(this);
    }

    /// <summary>
    /// Fonction donnant une impulsion dans une direction donnée, cette fonction est appelée dans le GameManager
    /// </summary>
    /// <param name="direction"></param>
    public void DoImpulse(Direction direction)
    {
        Vector3 impulseDirection = Vector3.zero;
        float angle = 0;
        bool jump = false;

        switch (direction)
        {
            case Direction.left:
                impulseDirection = Vector3.left * force;
                angle = 270;
                break;
            case Direction.right:
                impulseDirection = Vector3.right * force;
                angle = 90;
                break;
            case Direction.up:
                impulseDirection = Vector3.up * force;
                angle = 180;
                break;
            case Direction.down:
                impulseDirection = Vector3.down * force;
                angle = 0;
                break;
            case Direction.jump:
                if (transform.position.z > 0f)
                    impulseDirection = Vector3.back * jumpForce;
                jump = true;
                break;
        }
        rb.AddForce(impulseDirection, ForceMode.Impulse);
        timerInactivity = 0;//remise à zéro du timer d'inactivité
        if (!jump)//on ne veux pas que le mesh pivote quand on saute
        {
            Go_SbireMesh.transform.eulerAngles = new Vector3(0, 0, angle);
            
        }
    }

    /// <summary>
    /// Fonction et animation en dotween pour respawn le joueur
    /// </summary>
    /// <returns></returns>
    public IEnumerator DoRespawn()
    {
        rb.useGravity = false;

        yield return new WaitForSeconds(3f);

        transform.position = transform.GetComponentInParent<SpawnOverWebSocket>().SpawnPoint();
        rb.velocity = Vector3.zero;
        rb.useGravity = true;
        GetComponent<TrailRenderer>().Clear();

        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSeconds(0.2f);
            mesh.enabled = false;
            yield return new WaitForSeconds(0.2f);
            mesh.enabled = true;
        }

        
        doRespawn = null;
    }
}
