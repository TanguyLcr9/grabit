﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnitySocketIO;
using UnitySocketIO.Events;

public class GameManager : MonoBehaviour
{
    SocketIOController io;
    public static GameManager Instance;

    public TextMeshProUGUI TimeDisplay;
    public float Lifes;//
    public GameObject EndPanel;
    public List<SbireBehaviour> Sbires_List;//stocker les joueurs sur le réseau
    public TextMeshProUGUI EndText;

    float timer;
    bool endGame;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        EndPanel.gameObject.SetActive(false);
        io = SocketIOController.instance;
        io.On("connect", (SocketIOEvent e) => {
            Debug.Log("SocketIO connected");
        });

        io.Connect();

        //évenement du boutton haut
        io.On("up", (SocketIOEvent e) => {
            if (Sbires_List.Find(x => x.gameObject.name == e.data) != null)//chercher le joueur en question dans la liste s'il existe
            {
                Sbires_List.Find(x => x.gameObject.name == e.data).DoImpulse(Direction.up);
            }
        });

        //évenement du boutton bas
        io.On("down", (SocketIOEvent e) => {
            if(Sbires_List.Find(x => x.gameObject.name == e.data) != null)//chercher le joueur en question dans la liste s'il existe
            {
                Sbires_List.Find(x => x.gameObject.name == e.data).DoImpulse(Direction.down);
            }
        });

        //évenement du boutton droite
        io.On("right", (SocketIOEvent e) => {
            if(Sbires_List.Find(x => x.gameObject.name == e.data) != null)//chercher le joueur en question dans la liste s'il existe
            {
                Sbires_List.Find(x => x.gameObject.name == e.data).DoImpulse(Direction.right);
            }
        });

        //évenement du boutton gauche
        io.On("left", (SocketIOEvent e) => {
            if (Sbires_List.Find(x => x.gameObject.name == e.data) != null)//chercher le joueur en question dans la liste s'il existe
            {
                Sbires_List.Find(x => x.gameObject.name == e.data).DoImpulse(Direction.left);
            }
        });

        //évenement du boutton jump
        io.On("jump", (SocketIOEvent e) => {

            if(Sbires_List.Find(x => x.gameObject.name == e.data) != null)//chercher le joueur en question dans la liste s'il existe
            {
                    Sbires_List.Find(x => x.gameObject.name == e.data).DoImpulse(Direction.jump);
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        if (endGame)
        {
            if(Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3") || Input.GetButtonDown("Jump")) {
                SceneManager.LoadScene(0);//recharger la scène si le joueur central clique
            }
        }
        else
        {
            timer += Time.deltaTime;
            TimeDisplay.text = "time : " + (int)timer;
        }
    }

    /// <summary>
    /// Fonction d'incrémentation / décrémentation de la vie du joueur central symbolisé par les bouttons clignotants dans le jeu
    /// </summary>
    /// <param name="increment"></param>
    public void AddToLife(int increment)
    {
        Lifes--;
        if (Lifes <= 0)//evenement de fin
        {
            TimeDisplay.enabled = false;
            EndPanel.SetActive(true);
            EndText.text = "Vous avec vaincu le boss en <color=#f54242>" + (int)timer;
            BonusSpawner.Instance.gameObject.SetActive(false);
            endGame = true;
        }
    }
}
