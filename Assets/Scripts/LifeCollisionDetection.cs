﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LifeCollisionDetection : MonoBehaviour
{
    private void Start()
    {
        GameManager.Instance.Lifes++;
        StartCoroutine(DoSwitchColor());
    }

    /// <summary>
    /// Effet de blink pour rendre les bouttons plus dynamiques
    /// </summary>
    /// <returns></returns>
    IEnumerator DoSwitchColor()
    {
        while (true)
        {
            GetComponent<Renderer>().material.color = Color.red;
            GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);
            yield return new WaitForSeconds(1f);
            GetComponent<Renderer>().material.color = Color.yellow;
            GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.yellow);
            yield return new WaitForSeconds(1f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sbire"))
        {
            //Destroy Life if sbire is trigger
            GameManager.Instance.AddToLife(-1);
            Destroy(gameObject);
        }
    }
}
