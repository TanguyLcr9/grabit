﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BonusDetection : MonoBehaviour
{
    public BonusType type;
    public float maxTimer = 5f;

    float timer;
    Coroutine DoDie;

    private void Start()
    {
        //effet de zoom quand le bonus spawn
        Vector3 scaleSave = transform.localScale;
        transform.localScale = Vector3.zero;
        transform.DOScale(scaleSave,1f);
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > maxTimer && DoDie == null)
        {
            DoDie = StartCoroutine(DestroyingBonus());
        }
    }

    /// <summary>
    /// Effet de dézoom quand le bonus est terminé
    /// </summary>
    /// <returns></returns>
    IEnumerator DestroyingBonus()
    {
        yield return transform.DOScale(Vector3.zero, 1f).WaitForCompletion();

        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sbire"))//Les sbires peuvent récuperer uniquement les malus (en Rouge)
        {
            
            switch (type)
            {
                
                case BonusType.Slower:
                    MoulinBehaviour.Instance.ChangeSpeed(-4f);
                    Destroy(gameObject);
                    break;
                case BonusType.Shrink:
                    MoulinBehaviour.Instance.ChangeSize(-2.5f);
                    Destroy(gameObject);
                    break;
            }
        }
        
        if (other.CompareTag("Grabber"))//Le joueur central peut récupérer uniquement les bonus (en Verts)
        {
            
            switch (type)
            {
                case BonusType.Faster:
                    MoulinBehaviour.Instance.ChangeSpeed(4f);
                    Destroy(gameObject);
                    break;
                case BonusType.Grow:
                    MoulinBehaviour.Instance.ChangeSize(2.5f);
                    Destroy(gameObject);
                    break;
            }
        }
    }
}
