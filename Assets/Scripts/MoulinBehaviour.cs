﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoulinBehaviour : MonoBehaviour
{
    public static MoulinBehaviour Instance;
    public float speed = 1;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0,0,Time.deltaTime * speed));
    }

    /// <summary>
    /// Changer la taille des pales du moulin avec une animation
    /// </summary>
    /// <param name="value"></param>
    public void ChangeSize(float value)
    {
       
        for(int i=0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);

            if(child.localScale.y > 7.5f && child.localScale.y < 17.5f)//taille max et minimale des enfants
            child.DOScaleY(child.localScale.y + value, 0.5f);//changement dynamique de la taille
        }
    }

    /// <summary>
    /// Modifier la vitesse des pales du moulin
    /// </summary>
    /// <param name="value"></param>
    public void ChangeSpeed(float value)
    {
        speed += value;
        Debug.Log(speed);
        speed = Mathf.Clamp(speed, 3f, 40f);
        Debug.Log(speed);
    }
}
