// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(path.join(__dirname, 'public')));

// Chatroom

var numUsers = 0;

io.on('connection', (socket) => {
  var addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', (data) => {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', (username) => {
    if (addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });
    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
  });

  socket.on('pressed', function(msg){
    console.log('message from user ' + msg.pseudo);
    io.emit("pressed",msg.pseudo);
  });

  socket.on('spawn', function(msg){
    console.log('message from user ' + msg.pseudo);
    io.emit("spawn",msg.pseudo);
  });
  
  socket.on('up', function(msg){
    console.log('user ' + msg.pseudo+" has pressed up");
    io.emit("up",msg.pseudo);
  });
  
  socket.on('down', function(msg){
    console.log('user ' + msg.pseudo+" has pressed down");
    io.emit("down",msg.pseudo);
  });

  socket.on('right', function(msg){
    console.log('user ' + msg.pseudo+" has pressed right");
    io.emit("right",msg.pseudo);
  });
  
  socket.on('left', function(msg){
    console.log('user ' + msg.pseudo+" has pressed left");
    io.emit("left",msg.pseudo);
  });

  socket.on('jump', function(msg){
    console.log('user ' + msg.pseudo+" has pressed jump");
    io.emit("jump",msg.pseudo);
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    if (addedUser) {
      --numUsers;

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });
});