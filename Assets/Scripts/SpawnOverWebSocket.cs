﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySocketIO;
using UnitySocketIO.Events;
using TMPro;

public class SpawnOverWebSocket : MonoBehaviour
{
    public GameObject objectToSpawn;
    [Range(0,30)]
    public float RangeMax, RangeMin;

    SocketIOController io;

    // Start is called before the first frame update
    void Start()
    {
        io = SocketIOController.instance;
        io.On("connect", (SocketIOEvent e) => {
            Debug.Log("SocketIO connected");
        });

        io.Connect();

        //evenement de spawn de joueur
        io.On("spawn", (SocketIOEvent e) => {
            Debug.Log(e.data);
            if (GameObject.Find(e.data) == null)
            {

                string newName = e.data.Trim('"');
                if (newName.Length >= 10)
                {
                    newName = newName.Substring(0, 10);
                }
                GameObject newInstance = Instantiate(objectToSpawn, SpawnPoint(), Quaternion.identity);

                newInstance.gameObject.name = e.data;//changement du nom de l'objet en accord avec le e.data pour pouvoir le retrouver
                newInstance.transform.GetChild(0).transform.GetChild(0).GetComponent<TextMeshPro>().text = newName;//Changement du texte affiché de l'objet
                newInstance.transform.SetParent(transform);//rassembler sous le même objet parent

                GameManager.Instance.Sbires_List.Add(newInstance.GetComponent<SbireBehaviour>());//Ajouter à la liste des joueurs sur le gameManager
            }
        });
    }

    /// <summary>
    /// Définir un point de spawn entre un dimètre minimum et maximum
    /// </summary>
    /// <returns></returns>
    public Vector3 SpawnPoint()
    {
        Vector3 spawnPoint = new Vector3(RangeMax,0,0);
        float RanPosX, RanPosY;

        for (int i = 0; i <= 50; i++)
        {
            RanPosX = Random.Range(-RangeMax, RangeMax);
            RanPosY = Random.Range(-RangeMax, RangeMax);

            if (RangeMin < Vector3.Distance(Vector3.zero, new Vector3(RanPosX, RanPosY, 0))
                && Vector3.Distance(Vector3.zero, new Vector3(RanPosX, RanPosY,0)) < RangeMax)//Si le vecteur random est bien entre les deux cercles
            {
                spawnPoint = new Vector3(RanPosX, RanPosY, 0);//Alors celui-ci est bon
                break;
            }

            if (i == 49)//Si trop de calculs nécessaires alors on affiche un message
            {
                Debug.Log("Pas de place !!!, il faut laisser plus de place entre RangeMax et RangeMin");
            }
        }

        //On aurait pu faire avec un angle random et une distance random pour éviter d'avoir à faire une boucle for

        return spawnPoint;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, RangeMax);//cerlce jaune pour le maximum

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, RangeMin);//cerlce rouge pour le maximum

        //Clamper les valeurs
        RangeMin = Mathf.Clamp(RangeMin, 0, RangeMax);
        RangeMax = Mathf.Clamp(RangeMax, RangeMin, 30);
    }
}
