﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    public GameObject Go_Grabber;
    public Transform T_TargetGrabber;

    Coroutine grabbing;
    Vector3 joystick;

    
    void Update()
    {
        if (grabbing == null)// Si le gant n'es pas en train de grab
        {
            LookDirection();
        }

        if ((Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3") || Input.GetButtonDown("Jump")) && grabbing == null)//boutton A B X Y de la manette, et si le gant n'es pas déjà en train de grab
        {
            grabbing = StartCoroutine(DoGrab());
        }


    }

    /// <summary>
    /// Fonction déterminant la direction du gant de boxe par rapport au joystick
    /// </summary>
    public void LookDirection()
    {
        if (Input.GetAxis("JoyX") > 0.5f || Input.GetAxis("JoyY") > 0.5f || Input.GetAxis("JoyX") < -0.5f || Input.GetAxis("JoyY") < -0.5f)//Si le joystick fait un mouvement significatif
        {
            joystick = new Vector3(Input.GetAxis("JoyX"), Input.GetAxis("JoyY"), 0);
        }
        
        float angle = Vector3.Angle(joystick, Vector3.up);//l'angle de direction du joueur central
        if (joystick.x > 0)//si de l'autre côté il faut l'inversé
        {
            angle = -angle;
        }

        transform.GetChild(0).transform.eulerAngles = new Vector3(0, 0, angle);//on applique cet angle
    }

    /// <summary>
    /// Fonction engagent le gant de boxe vers le target
    /// </summary>
    /// <returns></returns>
    IEnumerator DoGrab()
    {
        yield return Go_Grabber.transform.DOMove(T_TargetGrabber.position, 1f).SetLoops(2,LoopType.Yoyo).WaitForCompletion();//Faire le mouvement jusqu'au target, revenir et attendre la fin du mouvement
        grabbing = null;
    }
}
