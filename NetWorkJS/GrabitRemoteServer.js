var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var count = 0

app.get('/', function(req, res){
	count++;
	res.sendFile(__dirname + '/index.html');
	//
});

var userId = 0;

io.on('connection', function(socket){
  socket.userId = userId ++;
  console.log('a user connected, user id: ' + socket.userId);

  socket.on('spawn', function(msg){
		console.log('message from user ' + msg.pseudo);
		io.emit("spawn",msg.pseudo);
  });
  
  socket.on('up', function(msg){
		console.log('user ' + msg.pseudo+" has pressed up");
		io.emit("up",msg.pseudo);
  });
  
  socket.on('down', function(msg){
		console.log('user ' + msg.pseudo+" has pressed down");
		io.emit("down",msg.pseudo);
  });

  socket.on('right', function(msg){
		console.log('user ' + msg.pseudo+" has pressed right");
		io.emit("right",msg.pseudo);
  });
  
  socket.on('left', function(msg){
		console.log('user ' + msg.pseudo+" has pressed left");
		io.emit("left",msg.pseudo);
  });

  socket.on('jump', function(msg){
		console.log('user ' + msg.pseudo+" has pressed jump");
		io.emit("jump",msg.pseudo);
  });

});


http.listen(3000, function(){
	console.log('listening on *:3000');
});