﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoLavaMove : MonoBehaviour
{
    public float forceEjection = 60f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DoTweenLavaMove());
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sbire"))
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.back * forceEjection, ForceMode.Impulse);//Ejecte avec une grande force le joueur dans les airs. Attention la lave ça brule les fesses
        }
    }

    /// <summary>
    /// Faire bouger la lave très légèrement
    /// </summary>
    /// <returns></returns>
    IEnumerator DoTweenLavaMove()
    {
        while (true)
        {
            yield return transform.DOMoveZ(1.9f, Random.Range(2f,4f)).SetLoops(2,LoopType.Yoyo).WaitForCompletion();
        }
    }
}
